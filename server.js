const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const app = express();
const port = 8080;
const folder = './files/';
const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

if (!fs.existsSync(folder)) {
  fs.mkdirSync(folder);
}

app.post('/api/files', (req, res) => {
  if (!req.body.content) {
    res.status(400).json({ message: "Please specify 'content' parameter" });
    console.log("Please specify 'content' parameter");
    return;
  }

  fs.writeFile(`files/${req.body.filename}`, req.body.content, (err) => {
    if (err) {
      res.status(500).json({ message: 'Server error' });
      console.log('Server error');
    }

    res.status(200).json({ message: 'File created successfully' });
    console.log('File created successfully');
  });
});

app.get('/api/files', (req, res) => {
  fs.readdir(folder, (err, data) => {
    if (err) {
      res.status(500).json({ message: 'Server error' });
      console.log('Server error');
    }

    if (data === []) {
      res.status(400).json({ message: 'Client error' });
      console.log('Client error');
    } else {
      res.status(200).json({ message: 'Success', files: data });
      console.log('Success');
    }
  });
});

app.get('/api/files/:filename', (req, res) => {
  const fileName = req.params.filename;
  const filePath = folder + fileName;
  const fileExtension = path.extname(fileName).substring(1);

  fs.readFile(filePath, 'utf-8', (err, data) => {
    if (err) {
      res
        .status(500)
        .json({ message: `No file with '${fileName}' filename found` });
      console.log(`No file with '${fileName}' filename found`);
      return;
    }

    fs.stat(filePath, (err, stats) => {
      if (err) {
        res.status(500).json({ message: 'Server error' });
        return;
      }

      res.status(200).json({
        message: 'Success',
        filename: fileName,
        content: data,
        extension: fileExtension,
        uploadedDate: stats.birthtime,
      });
    });
  });
});

app.listen(port);
